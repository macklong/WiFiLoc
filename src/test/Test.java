package test;

import h3c.H3C_OID;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import com.obj.PointObj;

public class Test {

	
	public static void main(String[] args) {
		
		randomData();
	}

	public static ArrayList<PointObj> randomData() {
		ArrayList<PointObj> points = new ArrayList<PointObj>();		
		double d1 = 108.988428;
		double d2 = 108.988913;
		int a = (int) ((d2 - d1) * (Math.pow(10, 6)));
		Random random = new Random();
		for (int i = 0; i < 5; i++) {
			
			double temp = (double) random.nextInt(a) / (Math.pow(10, 6));
			// 四舍五入保留六位小数
			BigDecimal b = new BigDecimal(temp + d1);
			double res = b.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
			
			PointObj point = new PointObj();
			point.lon = res;
			points.add(point);
			System.out.println(res);
		}
		
		ArrayList<Double> lats = new ArrayList<Double>();
		double lat1 = 34.256046;
		double lat2 = 34.2516;
		int b = (int) ((lat1-lat2)*(Math.pow(10, 6)));
		for(int i = 0; i < 5; i++ ){
			double temp = (double) random.nextInt(b) / (Math.pow(10, 6));
			// 四舍五入保留六位小数
			BigDecimal b0 = new BigDecimal(temp + lat2);
			double res = b0.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
			lats.add(res);				
			//System.out.println(res);
		}
		Collections.sort(lats);
		for(int i = 0; i < 5; i++){
			points.get(i).lat=lats.get(i);
		}
//		for(int i = 0; i < 5; i++){
//			System.out.println(points.get(i).lng + "  " + points.get(i).lat);
//		}
		return points;
	}
}
