package com.listener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.commons.collections.map.StaticBucketMap;

import com.control.Points;
import com.dao.MacDao;
import com.dao.PhoneDao;
import com.dao.UserDao;
import com.obj.MacNum;
import com.obj.PhoneObj;
import com.obj.PointObj;
import com.obj.sort.ComparePoint;

/*ServletContext是一个web应用所共享的对象，在服务器启动时被创建，
 * 服务器关闭时才销毁，作用与应用的整个生命周期，每个WEB应用都用特定的ServletContext。
 * */

/**
 * 
 * 使用@WebListener注解将实现了ServletContextListener接口的MyServletContextListener标注为监听器
 */
@WebListener
public class SystemInit implements ServletContextListener {

	final Logger log = Logger.getLogger(this.getClass().getName());
	// points
	public static ArrayList<PointObj> points = new ArrayList<PointObj>();
	// phones
	public static ArrayList<PhoneObj> phones = new ArrayList<PhoneObj>();
	//用户类型统计结果
	public static ArrayList<MacNum> macNums = new ArrayList<MacNum>();
	
	public static int userTableState = -1; // user_infor用户表的变化状态标识
	public static int macTableState = -1; // macinfor表的变化状态标识

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		log.info("系统终止。。。");
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		log.info("系统初始化开始。。");
		// 获取根目录
		String root_path = arg0.getServletContext().getRealPath("/");

		//监控用户
		userTableListener();

		// 获取手机排行中的中英文对照数据
		PhoneDao phoneObj = new PhoneDao();
		phones = phoneObj.getPhoneinfor();

		System.out.println(root_path);
		log.info("系统初始化结束...");

	}

	private void userTableListener() {
		//定义线程来监控数据库
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				//定义一个死循环来监控数据库表的变化
				while (true) {
					UserDao userDao = new UserDao();
					int user = userDao.getTriggerState(1); // 这里的 1 表示监控的是user_infor表的变化
					if (userTableState != user) {
						System.out.println("get data ..");
						// 获得经纬度数据
						Points p = new Points();
						points = p.getPoints("74-AD-B7-7F-CD-DE");
						System.out.println(points.size());
						Collections.sort(points, new ComparePoint());
						userTableState = user; //保存user_infor表当前的状态
					}
					
					int mac = userDao.getTriggerState(2);// 这里的 2 表示监控的是macinfor表的变化	
					if(macTableState != mac){
						//从数据库获取统计数据
				    	MacDao macDao = new MacDao();
				    	macNums = macDao.getMACStatistic();
				    	macTableState = mac;//保存macinfor表当前的状态
					}
					
					try {
						Thread.sleep(60*1000);//一分钟轮询一次数据库中相应表的的变化
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				//	System.out.println("table listener....");
				}
			}
		});
		thread.start();

	}

}
