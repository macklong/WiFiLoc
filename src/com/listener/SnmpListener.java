package com.listener;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.snmp4j.smi.VariableBinding;

import com.dao.APDao;
import com.dao.UserDao;
import com.obj.UserObj;

import h3c.H3C_OID;
import h3c.SnmpReq;

public class SnmpListener {
	/* AP序列号的ASCII码十进制表示方式，如 "XJTU_WLAN" 表示如下 */
	private static String APName = ".20.50.49.48.50.51.53.65.48.82.88.67.49.49.67.48.48.48.48.54.51";

	/**
	 * 将十六进制字符串转为十进制字符串，如： 68 A0 F6 69 4E 48 转为104.160.246.105.78.72
	 * 
	 * @param str
	 *            要转换的字符串
	 * @return 转换后的结果字符串
	 */
	private String hexToInt(String str) {
		String res = "";// 定义要返回的结果
		String[] strs = str.split(" ");// 依据空格字符将字符串分割成字符串数组。
		for (int i = 0; i < strs.length; i++) {
			res += Integer.valueOf(strs[i], 16).toString();// 将值为十六进制的字符串转为值为十进制的字符串
			if (i < strs.length - 1)
				res += ".";
		}
		return res;
	}

	/**
	 * 获取网络设备的全名和版本
	 * 
	 * @return 网络设备的全名和版本
	 */
	public String getSysDescr() {
		SnmpReq sr = new SnmpReq();
		Vector<VariableBinding> vbs = sr.sendPDU(H3C_OID.sysDescr);
		VariableBinding vb = vbs.elementAt(0);// 只用一条返回信息，所以索引直接用0
		String res = vb.getVariable().toString();// 获取返回的信息
		return res;
	}

	/**
	 * 获取接入终端的ip地址列表
	 * 
	 * @return 返回ip地址列表
	 */
	public ArrayList<String> getIPAddresses() {
		SnmpReq sr = new SnmpReq();
		Vector<VariableBinding> vbs = sr
				.sendPDU(H3C_OID.ipNetToMediaNetAddress);
		ArrayList<String> res = new ArrayList<String>();// 要返回的结果变量
		for (int i = 0; i < vbs.size(); i++) {
			VariableBinding vb = vbs.elementAt(i);
			res.add(vb.getVariable().toString());
		}
		return res;
	}

	/**
	 * 获取接入终端的MAC地址列表
	 * 
	 * @return 返回MAC地址列表
	 */
	public ArrayList<String> getMACAddresses() {
		SnmpReq sr = new SnmpReq();
		Vector<VariableBinding> vbs = sr
				.sendPDU(H3C_OID.ipNetToMediaPhysAddress);
		ArrayList<String> res = new ArrayList<String>();// 要返回的结果变量
		for (int i = 0; i < vbs.size(); i++) {
			VariableBinding vb = vbs.elementAt(i);
			res.add(vb.getVariable().toString());
		}
		return res;
	}

	/**
	 * 获取接入AP的MAC地址列表
	 * 
	 * @return 返回MAC地址列表
	 */
	public ArrayList<String> getAPMACs() {
		SnmpReq sr = new SnmpReq();
		Vector<VariableBinding> vbs = sr
				.sendPDU(H3C_OID.h3cDot11CurrAPMacAddress);
		ArrayList<String> res = new ArrayList<String>();// 要返回的结果变量
		for (int i = 0; i < vbs.size(); i++) {
			VariableBinding vb = vbs.elementAt(i);
			res.add(vb.getVariable().toString());
		}
		return res;
	}

	/**
	 * 获取AP Element ID
	 * 
	 * @return
	 */
	private int getAPElementID() {
		SnmpReq sr = new SnmpReq();
		Vector<VariableBinding> vbs = sr
				.sendPDU(H3C_OID.h3cDot11CurrAPElementID + APName); // ID加上名称才是需要ID的oid
		VariableBinding vb = vbs.elementAt(0);
		int res = vb.getVariable().toInt();
		return res;
	}

	/**
	 * 获取对应APmac地址的rssi值
	 * 
	 * @param mac
	 *            AP的mac地址
	 * @return
	 */
	public int getRSSI(String mac) {
		SnmpReq sr = new SnmpReq();
		// 将 68 A0 F6 69 4E 48 格式mac地址转为104.160.246.105.78.72格式
		String apMAC = hexToInt(mac);
		// 这里的oid应该为h3cDot11RrmNbrRSSI+APElementID+ apMAC
		Vector<VariableBinding> vbs = sr.sendPDU(H3C_OID.h3cDot11RrmNbrRSSI
				+ getAPElementID() + apMAC);
		VariableBinding vb = vbs.elementAt(0);
		int res = vb.getVariable().toInt();// 获取RSSI值，应该为负值
		return res;
	}

	/**
	 * 创建线程池来获取数据
	 */
	private void snmpExecute() {
		// 创建一个可重用固定线程数的线程池
		ExecutorService pool = Executors.newFixedThreadPool(10);
		ArrayList<String> macs = getMACAddresses();
		int SIZE = macs.size();
		Thread[] t = new Thread[SIZE];
		for (int i = 0; i < SIZE; i++) {
			t[i] = new ListenThread(macs.get(i));
			pool.execute(t[i]);// 将新建线程加入线程池
		}

	}

	/**
	 * 死循环监控AC，定时执行函数获取数据
	 */
	public void snmpListening() {
		while (true) {
			snmpExecute();
			try {
				TimeUnit.SECONDS.sleep(8);// 循环每8秒执行一次
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * 自定义监听线程，用来定时获取信息
	 * 
	 * @author ZYK
	 */
	class ListenThread extends Thread {
		private String mac;

		public ListenThread(String mac) {
			this.mac = mac;
		}

		@Override
		public void run() {
			APDao apDao = new APDao();
			UserObj userObj = new UserObj(); // 定义一个新的用户对象
			userObj.apID = apDao.getAPID(mac);// 获取AP对应的ID
			userObj.UserMAC = mac;
			userObj.rssi = getRSSI(mac);
			Date date = new Date(); // 获取当前系统时间 如 ：2015-04-27 16:32:51
			Timestamp timestamp = new Timestamp(date.getTime());
			userObj.currentTime = timestamp;

			// 向数据库中插入相应的数据
			UserDao userDao = new UserDao();
			userDao.insert(userObj);
		}
	}
}