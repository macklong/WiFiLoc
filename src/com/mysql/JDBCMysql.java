package com.mysql;

import java.sql.*;


public class JDBCMysql {
	
	/*驱动程序名*/
	public String mysqlDriver = "com.mysql.jdbc.Driver";
	
	/* 要访问的数据库路径URL*/
	/*其中mysql表明使用的是MySQL数据库，后面的是主机地址和端口号，然后是要连接的数据库名称，
	 * 最后设置编码，这点非常重要，特别是数据库操作时有中文字符的时候。如果不设置编码查询或者插入时很可能出现乱码*/
	private String url = "jdbc:mysql://127.0.0.1:3306/db_snmp?useUnicode=true&characterEncoding=utf8";
	
	/*MySQL配置是的用户名*/
	private String user = "root";
	
	/*Java连接MySQL数据库时的密码*/
	private String password = "mzzyk";
	
	/**
	 * @return MySQL数据库的连接情况
	 */
	public  Connection  getConn() {
		
		Connection conn = null;
		try {
			//加载MySQL驱动程序
			Class.forName(mysqlDriver);
			//连接数据库
			conn = DriverManager.getConnection(url, user, password);
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("MySQL数据库连接出现异常。。");
			e.printStackTrace();
		}
		return conn;
	}
	public static void main(String args[]){
		//测试MySQL连接状态
		JDBCMysql mysql =new JDBCMysql();
		Connection res = mysql.getConn();
		try {
			//判断数据库是否连接成功
			if(!res.isClosed())
				System.out.println("MySQL数据连接成功");
			else
				System.out.println("数据连接失败！");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
