package com.dist;

import java.util.Random;

import com.obj.PointObj;

/**确定位置的基本思想
 * * 对于两个点，在纬度相等的情况下：  经度每隔0.00001度，距离相差约1米；每隔0.0001度，距离相差约10米；
 * 每隔0.001度，距离相差约100米；每隔0.01度，距离相差约1000米；每隔0.1度，距离相差约10000米。 对于两个点，
 * 在经度相等的情况下：    纬度每隔0.00001度，距离相差约1.1米；每隔0.0001度，距离相差约11米；
 * 每隔0.001度，距离相差约111米；每隔0.01度，距离相差约1113米；每隔0.1度，距离相差约11132米。
 * 
 * @author ZYK
 *
 */
public class Location {
	
	public PointObj getLocation(PointObj p,double rssi){
		PointObj res = new PointObj() ;
		//求得p1和p2俩点之间的距离，可得一个x^2 + y^2 =dist^2 的圆，在该圆边上随机一个点的坐标
		//将随机点的坐标以AP点的经纬度坐标为基准进行转换求得真是坐标
		double dist = RSSIToDist.rssiToDistance(rssi);
		//随机一个x 的值，然后依据方程求得相应的y 的值。
		double r = dist;
		if (dist > 5.0){
			r = 5.0;
		}
		double x = Math.random()*r;
		double y = Math.sqrt(Math.pow(dist, 2) - Math.pow(x, 2));
		
		Random rand = new Random();
		int flag = rand.nextInt(4);
		
//		if(flag == 1){
//			x = -x;
//		}
//		if(flag == 2){
//			x = -x;
//			y = -y;
//		}
//		if(flag ==3){
//			y = -y;
//		}
		//进行坐标转换，确定位置
	//	System.out.println(x +" "+y);
		res.lon = SpecialData.getData(p.lon + x * 0.00001,6);
		res.lat = SpecialData.getData(p.lat + (y / 1.1) * 0.00001, 6);
		
		return res;
	}
	public static void main(String argsString []){
		Location location = new Location();
		//108.988684, 34.255505
		PointObj p = new PointObj();
		p.lon = 108.988684;
		p.lat = 34.255505;
		PointObj res = location.getLocation(p, -20);
		System.out.println(res.lon + " " + res.lat);
	}
}