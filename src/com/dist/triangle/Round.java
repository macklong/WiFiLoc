package com.dist.triangle;

/**
 * @author ZYK
 * 圆的标准方程为 （x-a）^2 + (y - b)^2 = r^2 
 */
public class Round {

	/**
	 * 圆心坐标Ｘ
	 */
	public double a;
	/**
	 * 圆心坐标Y
	 */
	public double b;
	/**
	 * 圆的半径R
	 */
	public double r;
	
	public Round () {
		
	}
	public Round(double a,double b,double r) {
		this.a = a;
		this.b = b;
		this.r = r;
	}
}
