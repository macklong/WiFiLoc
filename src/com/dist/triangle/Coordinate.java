package com.dist.triangle;

/**
 * @author ZYK
 *定义平面直角坐标系的横纵坐标
 */
public class Coordinate {

	/**
	 * 横坐标X
	 */
	public double x;
	/**
	 * 纵坐标Y
	 */
	public double y;
	/**
	 * 默认构造函数
	 */
	public Coordinate() {
		
	}
	/**
	 * 拷贝构造函数
	 * @param x 横坐标
	 * @param y 纵坐标
	 */
	public Coordinate(double x, double y) {
		this.x = x;
		this.y = y;
	}
}
