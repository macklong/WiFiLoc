package com.dist.triangle;

import java.util.ArrayList;

import sun.net.www.content.audio.x_aiff;

import com.dist.LatLonToDis;
import com.dist.RSSIToDist;
import com.dist.SpecialData;
import com.obj.PointObj;
import com.sun.org.apache.bcel.internal.generic.NEW;

public class TriangleLoc {

	/**
	 * 在经度相同的情况想，纬度没相差0.00001 距离相差1.1米
	 */
	private static double LAT_M = 0.00001;
	/**
	 * 在纬度相同的情况想，经度每相差0.00001 距离相差1米
	 */
	private static double LON_M = 0.00001;

	/**
	 * 通过输入一个看做坐标原点的AP经纬度和要计算的另一个AP的经纬度，来计算这个AP在相对的平面坐标系中的坐标（x,y）
	 * 
	 * @param opoint
	 *            以其中一个AP位置为坐标原点的经纬度坐标
	 * @param ipoint
	 *            要计算的另一AP的的经纬度坐标
	 * @return 要计算的另一AP的相对平面坐标
	 */
	private Coordinate getCoordinate(PointObj opoint, PointObj ipoint) {
		Coordinate c = new Coordinate();
		c.x = (ipoint.lon - opoint.lon) / LON_M * 1;
		c.y = (ipoint.lat - opoint.lat) / LAT_M * 1.1;
		return c;
	}

	/**
	 * 求平方
	 * 
	 * @param d
	 * @return d*d
	 */
	private double P(double d) {
		return Math.pow(d, 2);
	}

	/**
	 * a*x^2 + bx + c =0 已知各个常数 a,b,c 求解一元二次方程的值
	 * 
	 * @param a
	 * @param b
	 * @param c
	 * @return
	 */
	private ArrayList<Double> getResX(double a, double b, double c) {
		// 一元二次方程的解为： x = -b + sqrt(b^2 - 4*a*c) 或者 x = -b -sqrt(b^2 - 4*a*c)
		ArrayList<Double> res = new ArrayList<Double>();
		double x1 = (-b + Math.sqrt(P(b) - 4 * a * c)) / (2 * a);
		double x2 = (-b - Math.sqrt(P(b) - 4 * a * c)) / (2 * a);
		res.add(x1);
		res.add(x2);
		return res;
	}

	/**
	 * 通过求解二元二次方程组来求得俩个圆的交点坐标
	 * 
	 * @param p1
	 *            圆一的主要参数
	 * @param p2
	 *            圆二的主要参数
	 * @return
	 */
	public ArrayList<Coordinate> getNode(Round p1, Round p2) {
		ArrayList<Coordinate> res = new ArrayList<Coordinate>();
		// 圆一的参数如下：（x - a1）^2 + (y - b1)^2 = d1^2; ....1
		double a1 = p1.a;
		double b1 = p1.b;
		double d1 = p1.r;
		// 圆二的参数如下：（x - a2）^2 + (y - b2)^2 = d2^2; .....2
		double a2 = p2.a;
		double b2 = p2.b;
		double d2 = p2.r;
		// 用来存储计算的结果
		Coordinate c1 = new Coordinate();
		Coordinate c2 = new Coordinate();
		// 联立方程1和方程2化简可得
		// y * 2*(b2-b1)= ((d1^2 - d2^2)-(a1^2 - a2^2)-(b1^2 -
		// b2^2)-2*(a2-a1)*x);
		// 依据化简结果将将常数部分用变量代替为 B*x +C*y =A....3
		double A = (P(d1) - P(d2)) - (P(a1) - P(a2)) - (P(b1) - P(b2));
		double B = 2 * (a2 - a1);
		double C = 2 * (b2 - b1);

		double a = 0;
		double b = 0;
		double c = 0;

		// 当 C = 0时，方程3式变为：x = A/B; 。。。3
		// 联立方程1 可得： y^2 - 2*b1*y + b1^2 +(A/B - a1)^2 - d1^2=0
		// 用变量代替为 a*x^2 +bx + c = 0;

		if (C == 0) {
			System.out.println("c == 0");
			a = 1;
			b = -2 * b1;
			c = P(b1) + P(A / B - a1) - P(d1);
			ArrayList<Double> y = new ArrayList<Double>();
			y = getResX(a, b, c);
			c1.x = c2.x = A / B;
			c1.y = y.get(0);
			c2.y = y.get(1);

		}
		// 当 B = 0时，3式变为：y = A/C;
		// 联立方程1 可得： x^2 - 2*a1*x + a1^2 +(A/C - b1)^2 - d1^2=0
		// 用变量代替为 a*x^2 +bx + c = 0;
		else if (B == 0) {
			System.out.println("b==0");
			a = 1;
			b = -2 * a1;
			c = P(a1) + P(A / C - b1) - P(d1);
			ArrayList<Double> x = new ArrayList<Double>();
			x = getResX(a, b, c);
			c1.y = c2.y = A / C;
			c1.x = x.get(0);
			c2.x = x.get(1);
		} else {
			// 将方程三带入方程1 可得关于 X 的一元二次方程，得：
			// (1 + (C/B)^2) * x^2 -2 * (a1＋ (A/B - b1)* C/B) * x + (A/B - b1)^2
			// +
			// a1^2 -d1^2 = 0;
			// 用变量代替为 a*x^2 +bx + c = 0;
			a = (1 + P(C / B));
			b = 2 * (a1 + (A / B - b1) * (C / B));
			c = P(A / B - b1) + P(a1) - P(d1);
			ArrayList<Double> x = new ArrayList<Double>();
			x = getResX(a, b, c);
			// 依据方程3 可得y 的值
			double y1 = (A - C * x.get(0)) / B;
			double y2 = (A - C * x.get(1)) / B;
			c1.x = x.get(0);
			c1.y = y1;
			c2.x = x.get(1);
			c2.y = y2;
		}
		res.add(c1);
		res.add(c2);
		return res;
	}

	/**
	 * 分别比较c1到c3的距离和c2到c3的距离，从c1和c2中选取近的一个返回
	 * 
	 * @param c1
	 *            第一个平面坐标
	 * @param c2
	 *            第二个平面坐标
	 * @param c3
	 *            第三个平面坐标
	 * @return c1 和c2 中离 c3 近的一个
	 */
	private Coordinate compareDis(Coordinate c1, Coordinate c2, Coordinate c3) {
		double a = P(c1.x - c3.x) + P(c1.y - c3.y);
		double b = P(c2.x - c3.x) + P(c2.y - c3.y);
		if (a > b) {
			return c2;
		} else
			return c1;
	}

	/**
	 * 通过三角形的三个顶点坐标数组求得该三角形的质心（重心）坐标
	 * 
	 * @param rounds
	 *            一个三角形的顶点坐标数组
	 * @return 三角形的质心坐标
	 */
	private Coordinate getCentroidXY(ArrayList<Coordinate> coordinates) {
		Coordinate res = new Coordinate();
		double x_sum = 0;
		double y_sum = 0;
		for (int i = 0; i < coordinates.size(); i++) {
			x_sum += coordinates.get(i).x;
			y_sum += coordinates.get(i).y;
		}
		res.x = x_sum / 3;
		res.y = y_sum / 3;
		return res;
	}

	/**
	 * 给定三点的地理位置坐标（经纬度），然后计算出该三点组成三角形的质心地理位置坐标
	 * 
	 * @param points
	 *            三个点的坐标的数组
	 * @return
	 */
	public PointObj getCentroid(ArrayList<PointObj> points) {
		PointObj res = new PointObj();

		// 默认以经纬度数组中第一组作为坐标的原点
		// 定义圆的参数数组
		ArrayList<Round> rounds = new ArrayList<Round>();
		Round r = new Round();
		r.a = 0;
		r.b = 0;
		r.r = RSSIToDist.rssiToDistance(points.get(0).rssi);
		rounds.add(r);
		// 依次将经纬度坐标转换为平面直角坐标，并以圆的参数形式存储
		for (int i = 1; i < 3; i++) {
			Coordinate co = new Coordinate();
			co = getCoordinate(points.get(0), points.get(i));
			Round round = new Round();
			round.a = co.x;
			round.b = co.y;
			round.r = RSSIToDist.rssiToDistance(points.get(i).rssi);
			rounds.add(round);
		}
		// 验证数据是否符合条件，任意俩个圆是否相交，即半径之和是否大于俩圆心距离
		// d1(1,2) d2(1,3) d3(2,3)
		PointObj p1 = new PointObj();
		PointObj p2 = new PointObj();
		PointObj p3 = new PointObj();
		p1 = points.get(0);
		p2 = points.get(1);
		p3 = points.get(2);

		// 计算三个圆俩俩之间的圆心距离；
		double d1 = LatLonToDis.getDis(p1.lon, p1.lat, p2.lon, p2.lat);
		double d2 = LatLonToDis.getDis(p1.lon, p1.lat, p3.lon, p3.lat);
		double d3 = LatLonToDis.getDis(p2.lon, p2.lat, p3.lon, p3.lat);
		// //计算三个圆俩俩之间的半径之和；
		Round r1 = new Round();
		Round r2 = new Round();
		Round r3 = new Round();
		r1 = rounds.get(0);
		r2 = rounds.get(1);
		r3 = rounds.get(2);
		double s1 = r1.r + r2.r;
		double s2 = r1.r + r3.r;
		double s3 = r2.r + r3.r;

		ArrayList<Coordinate> coors = new ArrayList<Coordinate>();
		// 如果俩个圆的半径之和小于圆心距离，说明俩个圆相离，这样的数据无效
		if (d1 > s1 || d2 > s2 || d3 > s3) {
			System.out.println("数据无效。。");
			res = null;
		} else {
			
			// 计算俩圆相交的交点以及判断交点中哪一个圆离第三个圆更近，近的点为三角形的一点
			int size = rounds.size();
			for (int i = 0; i < size; i++) {
				// 获取俩圆的交点坐标
				ArrayList<Coordinate> cos = new ArrayList<Coordinate>();
				cos = getNode(rounds.get(i), rounds.get((i + 1) % size));
				// 获取除了计算交点的俩个圆之外的第三个圆的圆心坐标
				Coordinate coor3 = new Coordinate();
				coor3.x = rounds.get((i + 2) % size).a;
				coor3.y = rounds.get((i + 2) % size).b;
				// 与第三个圆的圆心进行距离比较，选取较小的一个点
				Coordinate ct = new Coordinate();
				System.out.println(cos.get(0).x+ " "+ cos.get(0).y+" "+ cos.get(1).x+ " "+cos.get(1).y);
				ct = compareDis(cos.get(0), cos.get(1), coor3);
				coors.add(ct);
			}
			for (int i = 0; i < coors.size(); i++) {
				System.out.println("三角形坐标：  "+coors.get(i).x+ " "+coors.get(i).y);
			}
		
			// 计算质心坐标
			Coordinate res_Coordinate = new Coordinate();
			res_Coordinate = getCentroidXY(coors);			
			// 将质心坐标转换为地理坐标（经纬度）
			res.lon = points.get(0).lon + res_Coordinate.x * LON_M;
			res.lat = points.get(0).lat + (res_Coordinate.y / 1.1) * LAT_M;

			// 将计算结果保留六位小数
			res.lon = SpecialData.getData(res.lon, 6);
			res.lat = SpecialData.getData(res.lat, 6);
		}
		return res;
	}

	public static void main(String args[]) {

		PointObj p1 = new PointObj();
		p1.lon = 108.98880;
		p1.lat = 34.25560;
		p1.rssi = -60;
		PointObj p2 = new PointObj();
		p2.lon = 108.98883;
		p2.lat = 34.25560;
		p2.rssi = -60;
		PointObj p3 = new PointObj();
		p3.lon = 108.98882;
		p3.lat = 34.255632;
		p3.rssi = -60;
		ArrayList<PointObj> points = new ArrayList<PointObj>();
		points.add(p1);
		points.add(p2);
		points.add(p3);
		TriangleLoc triangleLoc = new TriangleLoc();
		PointObj res = new PointObj();
		res = triangleLoc.getCentroid(points);
		if (res != null)
			System.out.println("质心坐标是： " + res.lon + " " + res.lat);
		Round r1 = new Round(1,1,1.0);
		Round r2 = new Round(1,0,1.0); 
		ArrayList<Coordinate> resC = triangleLoc.getNode(r1, r2);
		
		System.out.println( resC.get(0).x +" "+ resC.get(0).y);
		System.out.println(resC.get(1).x +" "+ resC.get(1).y);
		System.out.println(RSSIToDist.rssiToDistance((double)-60));
		
	}
}
