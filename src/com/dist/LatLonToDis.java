package com.dist;

import java.util.ArrayList;

import com.obj.PointObj;

/**
 * 此类不在该项目中不使用
 * @author ZYK 通过俩点之间的经纬度来求得俩点之间的距离。
 */
public class LatLonToDis {

	// 赤道半径(单位m)
	private static final double EARTH_RADIUS = 6378137;

	/*主要的思想如下： 
	 * 设第一点A的经 纬度为(LonA, LatA)，第二点B的经纬度为(LonB, LatB)，
	 * 按照0度经线的基准，东经取经度的正值(Longitude)，西经取经度负值(-Longitude)， 北纬取90-纬度值(90-
	 * Latitude)，南纬取90+纬度值(90+Latitude)， 则经过上述处理过后的两点被计为(MLonA, MLatA)和(MLonB,
	 * MLatB)。那么根据三角推导，可以得到 计算两点距离的如下公式： C =
	 * sin(MLatA)*sin(MLatB)*cos(MLonA-MLonB) + cos(MLatA)*cos(MLatB) Distance =
	 * R*Arccos(C)*Pi/180
	 */
	
	/**
	 * 转化为弧度(rad)
	 * */
	private static double rad(double d) {
		return d * Math.PI / 180.0;
	}

	/**
	 * 基于余弦定理求两经纬度距离
	 * 
	 * @param lon1
	 *            第一点的经度
	 * @param lat1
	 *            第一点的纬度
	 * @param lon2
	 *            第二点的经度
	 * @param lat3
	 *            第二点的纬度
	 * @return 返回的距离，单位km
	 * */
	public static double LantitudeLongitudeDist(double lon1, double lat1,
			double lon2, double lat2) {
		// 将经纬度转换为弧度
		double radLat1 = rad(lat1);
		double radLat2 = rad(lat2);

		double radLon1 = rad(lon1);
		double radLon2 = rad(lon2);

		// 按照0度经线的基准,转换经纬度
		if (radLat1 < 0)
			radLat1 = Math.PI / 2 + Math.abs(radLat1);// south
		if (radLat1 > 0)
			radLat1 = Math.PI / 2 - Math.abs(radLat1);// north
		if (radLon1 < 0)
			radLon1 = Math.PI * 2 - Math.abs(radLon1);// west
		if (radLat2 < 0)
			radLat2 = Math.PI / 2 + Math.abs(radLat2);// south
		if (radLat2 > 0)
			radLat2 = Math.PI / 2 - Math.abs(radLat2);// north
		if (radLon2 < 0)
			radLon2 = Math.PI * 2 - Math.abs(radLon2);// west

		double x1 = EARTH_RADIUS * Math.cos(radLon1) * Math.sin(radLat1);
		double y1 = EARTH_RADIUS * Math.sin(radLon1) * Math.sin(radLat1);
		double z1 = EARTH_RADIUS * Math.cos(radLat1);

		double x2 = EARTH_RADIUS * Math.cos(radLon2) * Math.sin(radLat2);
		double y2 = EARTH_RADIUS * Math.sin(radLon2) * Math.sin(radLat2);
		double z2 = EARTH_RADIUS * Math.cos(radLat2);

		double d = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)
				+ (z1 - z2) * (z1 - z2));
		// 余弦定理求夹角
		double theta = Math.acos((EARTH_RADIUS * EARTH_RADIUS + EARTH_RADIUS
				* EARTH_RADIUS - d * d)
				/ (2 * EARTH_RADIUS * EARTH_RADIUS));
		double dist = theta * EARTH_RADIUS;
		//SpecialData.getData(dist, 2);
		return dist;
	}

	/*
	 * 鉴于该应用中俩点之间的距离比较近，所以可以采用如下算法，主要思想是利用求长方形的对角线的思想 关于经纬度十进制表示法
	 * 对于两个点，在纬度相等的情况下： 经度每隔0.00001度，距离相差约1米；每隔0.0001度，距离相差约10米；
	 * 每隔0.001度，距离相差约100米；每隔0.01度，距离相差约1000米；每隔0.1度，距离相差约10000米。
	 * 对于两个点，在经度相等的情况下： 纬度每隔0.00001度，距离相差约1.1米；每隔0.0001度，距离相差约11米；
	 * 每隔0.001度，距离相差约111米；每隔0.01度，距离相差约1113米；每隔0.1度，距离相差约11132米。
	 */

	/**
	 * @param lon1
	 *            第一个点的经度
	 * @param lat1
	 *            第一个点的纬度
	 * @param lon2
	 *            第二个点的经度
	 * @param lat2
	 *            第二个点的纬度
	 * @return 俩点之间的距离
	 */
	public static double getDis(double lon1, double lat1, double lon2,
			double lat2) {
		double dist = 0;
		double x = Math.abs(lon1 - lon2) / 0.00001;
		double y = (Math.abs(lat1 - lat2) / 0.00001) * 1.1;
	    dist = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
		//dist = SpecialData.getData(temp, 2);
		return dist;
	}

	public static void main(String[] args) {
		// 108.988684,34.255505,108.988617,34.255673
		// 108.988428, 34.256046, 108.988383, 34.2516
		PointObj p0 = new PointObj();
		p0.lon = 108.988620;
		p0.lat = 34.255673;
		ArrayList<PointObj>  points = new ArrayList<PointObj>();
		PointObj point = new PointObj();
		point.lon = 108.988685;
		point.lat = 34.255505;
		points.add(point);
		point = new PointObj();
		point.lon = 108.988670;
		point.lat = 34.255535;
		points.add(point);
		point = new PointObj();
		point.lon = 108.988655;
		point.lat = 34.255565;
		points.add(point);
		point = new PointObj();
		point.lon = 108.988640;
		point.lat = 34.255595;
		points.add(point);
		point = new PointObj();
		point.lon = 108.988630;
		point.lat = 34.255635;
		points.add(point);
		point = new PointObj();
		point.lon = 108.988625;
		point.lat = 34.255665;
		points.add(point);
		for (int i = 0; i < points.size() ; i++) {
			double dis = LatLonToDis.LantitudeLongitudeDist(points.get(i).lon, points.get(i).lat,
					108.988617, 34.255673);
			double res = LatLonToDis.getDis(points.get(i).lon,points.get(i).lat, 108.988617,
					34.255673);
			double a = SpecialData.getData(dis, 3);
			double b = SpecialData.getData(res, 3);
			double c =SpecialData.getData(a-b,3);
			System.out.println(a+ " "+b + " "+c);
		}
		
//		double dis = LatLonToDis.LantitudeLongitudeDist(108.988684, 34.255505,
//				108.988617, 34.255673);
//		System.out.println(dis);
//		double res = LatLonToDis.getDis(108.988684, 34.255505, 108.988617,
//				34.255673);
//		System.out.println(res);
	}

}
