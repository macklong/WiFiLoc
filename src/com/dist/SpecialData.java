package com.dist;

import java.math.BigDecimal;

public class SpecialData {
	
	/**
	 * 主要将一个给定的double型数据四舍五入保留特定的小数位
	 * @param temp 需要进行四舍五入的数据
	 * @param num  四舍五入要保留的小数位数
	 * @return  四舍五入的结果
	 */
	public static double getData(double temp,int num) {
		
		BigDecimal b = new BigDecimal(temp);
		double res = b.setScale(num, BigDecimal.ROUND_HALF_UP).doubleValue();
		return res;
	}
}
