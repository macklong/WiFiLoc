package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.JDBCMysql;
import com.obj.MacNum;

public class MacDao {

	/**
	 * 查询所有mac地址
	 * 
	 * @return mac地址列表
	 */
	public ArrayList<String> lsitMAC() {
		ArrayList<String> res = new ArrayList<String>();

		JDBCMysql dbUtil = new JDBCMysql();
		Connection conn = dbUtil.getConn();

		String sql = "select mac from user_mac";

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(sql);

			rs = ps.executeQuery();
			while (rs.next()) {
				res.add(rs.getString("mac"));
			}
		} catch (SQLException e) {
			System.out.println("SQL 语句异常。。。");
			e.printStackTrace();
		} finally {
			CloseConn.closeConn(conn, ps, rs);
		}
		return res;
	}

	public ArrayList<MacNum> getMACStatistic() {
		ArrayList<MacNum> res = new ArrayList<MacNum>();

		JDBCMysql dbUtil = new JDBCMysql();
		Connection conn = dbUtil.getConn();
		
		//以des列为准进行统计
		String sql = "select des,COUNT(*) as count from macinfor GROUP BY(des)";

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(sql);

			rs = ps.executeQuery();
			while (rs.next()) {
				MacNum mac = new MacNum();
				mac.ch_des = rs.getString("des");
				mac.count = rs.getInt("count");
				res.add(mac);
			}
		} catch (SQLException e) {
			System.out.println("SQL 语句异常。。。");
			e.printStackTrace();
		} finally {
			CloseConn.closeConn(conn, ps, rs);
		}
		return res;
	}
}
