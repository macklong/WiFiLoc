package com.dao;

import com.mysql.JDBCMysql;
import com.obj.APobj;
import com.obj.PointObj;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class APDao {

	/**
	 * 通过ap的mac地址来查询ap对应的id
	 * 
	 * @param apMac
	 *            AP的mac地址
	 * @return AP的标识
	 */
	public int getAPID(String apMac) {
		int id = -1;
		JDBCMysql dbUtil = new JDBCMysql();
		Connection conn = dbUtil.getConn();

		String sql = "select ap_id from ap_infor where ap_mac=?";

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, apMac);
			 rs = ps.executeQuery();
			while (rs.next()) {
				id = rs.getInt("ap_id");
			}
		} catch (SQLException e) {
			System.out.println("SQL 语句异常。。。");
			e.printStackTrace();
		}finally{
			CloseConn.closeConn(conn, ps, rs);
		}
		return id;
	}

	/**
	 * 通过用户表（user_infor）中的每一个用户所涉及的APid来查询这些AP的经纬度。
	 *  SELECT ap_infor.ap_id, ap_infor.ap_lat, ap_infor.ap_lon 
	 *  FROM ap_infor 
	 *  WHERE ap_id in 
	 *  ( SELECT DISTINCT user_infor.ap_id 
	 *  FROM user_infor WHERE
	 *  user_infor.user_mac="74-AD-B7-7F-CD-DE" )
	 * 
	 * @param ap_id
	 * @return longitude and latitude of ap
	 */
	public ArrayList<APobj> getApInfor(String mac) {
		ArrayList<APobj> objs = new ArrayList<APobj>();

		JDBCMysql dbUtil = new JDBCMysql();
		Connection conn = dbUtil.getConn();

		String sql = "select ap_id,ap_lon,ap_lat from ap_infor where ap_id in "
				+ "(select DISTINCT ap_id from user_infor where user_mac=?)";

		PreparedStatement ps =null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, mac);
			rs = ps.executeQuery();
			while (rs.next()) {
				APobj obj = new APobj();
				obj.id = rs.getInt("ap_id");
				obj.point.lon = (double)rs.getDouble("ap_lon");
				obj.point.lat = (double)rs.getDouble("ap_lat");
				objs.add(obj);
			}
		} catch (SQLException e) {
			System.out.println("SQL 语句异常。。。");
			e.printStackTrace();
		}
		finally{
			CloseConn.closeConn(conn, ps, rs);
		}
		return objs;
	}

	public PointObj getLonLat(int ap_id) {
		PointObj point = new PointObj();

		JDBCMysql dbUtil = new JDBCMysql();
		Connection conn = dbUtil.getConn();

		String sql = "select ap_lon,ap_lat from ap_infor where ap_id=?";

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, ap_id);
			rs = ps.executeQuery();
			while (rs.next()) {
				point.lon = rs.getDouble("ap_lon");
				point.lat = rs.getDouble("ap_lat");
			}
		} catch (SQLException e) {
			System.out.println("SQL 语句异常。。。");
			e.printStackTrace();
		}
		finally{
			CloseConn.closeConn(conn, ps, rs);
		}
		return point;
	}

}
