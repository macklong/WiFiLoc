package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.JDBCMysql;
import com.obj.MacObj;
import com.obj.PhoneObj;

public class PhoneDao {

	/**
	 * 最新的手机排行榜的中的前22个中英文对照
	 * 
	 * @return 中英文对照序列
	 */
	public ArrayList<PhoneObj> getPhoneinfor() {
		ArrayList<PhoneObj> res = new ArrayList<PhoneObj>();

		JDBCMysql dbUtil = new JDBCMysql();
		Connection conn = dbUtil.getConn();
		String sql = "select * from phone group by (weight)";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				PhoneObj obj = new PhoneObj();
				obj.en_des = rs.getString("en_des");
				obj.ch_des = rs.getString("ch_des");
				res.add(obj);
			}
		} catch (SQLException e) {
			System.out.println("SQL error");
			e.printStackTrace();
		} finally {
			CloseConn.closeConn(conn, ps, rs);
		}

		return res;
	}
	/**
	 * 批量插入，减缓数据库访问的压力
	 * @param objs 要插入的数据
	 * @return 插入操作成功与否的状态
	 */
	public boolean insert(ArrayList<MacObj> objs){
		boolean res = false;
		JDBCMysql dbUtil = new JDBCMysql();
		Connection conn = dbUtil.getConn();
		//这里的ignore 表示插入非重复的数据。如果数据重复则不插入
		String sql = "insert ignore  into macinfor(mac,des) values(?,?)";
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(sql);
			// 将连接的自动提交关闭，数据在传送到数据库的过程中相当耗时
			conn.setAutoCommit(false);
			for (int i = 0; i < objs.size(); i++) {
				ps.setString(1, objs.get(i).mac);
				ps.setString(2, objs.get(i).des);
				//将预处理添加到批中
				ps.addBatch();
			}
			//预处理批量执行
			ps.executeBatch();
			ps.clearBatch();
			conn.commit();
			//关闭自动提交事物，否则执行失败时，在catch中回滚数据时引发错误
			conn.setAutoCommit(true);
			
			//成功插入数据，改变状态
			res = true;
		} catch (SQLException e) {
			System.out.println("SQL error..");
			e.printStackTrace();
		}		
		
		return res;
	}
	public ArrayList<String> getMac() {
		ArrayList<String> res = new ArrayList<String>();

		JDBCMysql dbUtil = new JDBCMysql();
		Connection conn = dbUtil.getConn();
		String sql = "select * from db_mac";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()){
				res.add(rs.getString("mac"));
			}
		} catch (SQLException e) {
			System.out.println("SQL error!");
			e.printStackTrace();
		}		
		return res;
	}
	public static void main(String args[]){
		PhoneDao phoneObj = new PhoneDao();
		ArrayList<PhoneObj> objs = phoneObj.getPhoneinfor();
		for (int i = 0; i < objs.size(); i++) {
			System.out.println(objs.get(i).en_des + " " +objs.get(i).ch_des);
		}		
	}
}
