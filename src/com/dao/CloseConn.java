package com.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;

/**
 * @author ZYK
 *主要用来关闭和释放数据库操作占用的相关资源。
 */
public class CloseConn {

	/**
	 * 每一次数据库操作完成之后，应该关闭相应的资源，如connection，resultset preparedstatement等。
	 * 该操作应该在finally 中执行
	 * 如果不进行关闭操作，那么当连续几次请求数据库的时候，就会出现 “too many connections”的异常
	 * @param conn
	 * @param ps
	 * @param rs
	 */
	public static void closeConn(Connection conn,PreparedStatement ps, ResultSet rs){
		try {
			//都是先判断所占用的资源十分为空，然后进行关闭操作
			if (ps!=null)
				ps.close();
			if(conn != null)
				conn.close();
			if(rs != null)
				rs.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
