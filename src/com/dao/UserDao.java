package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;



import java.util.Date;

import com.mysql.JDBCMysql;
import com.obj.UserObj;

/**
 * @author ZYK 主要用来操作数据库，通过SQL语句进行查询和插入数据
 */
public class UserDao {

	/**
	 * @param mac
	 * @return 查询的结果，保存在一个list里面
	 */
	public ArrayList<UserObj> select(String user_mac) {
		// 定义一个保存查询结果数据的list
		ArrayList<UserObj> res = new ArrayList<UserObj>();
		// 新建一个数据库连接对象
		JDBCMysql dbUtil = new JDBCMysql();
		// 定义一个连接
		Connection conn = dbUtil.getConn();
		// 定义要操作的SQL语句；
		// String sql = "select * from db_mac where num=?";
		String sql = "select * from user_infor where user_mac=?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			// 准备好SQL语句
			ps = conn.prepareStatement(sql);
			// 设置查询条件
			 ps.setString(1, user_mac);
			// 定义数据库结果集的数据表，通常是通过执行查询数据库语句生成
			rs = ps.executeQuery();
			while (rs.next()) {
				UserObj obj = new UserObj();
				obj.apID = rs.getInt("ap_id");
				obj.UserMAC = rs.getString("user_mac");
				obj.currentTime = rs.getTimestamp("curr_time");
				obj.rssi = rs.getInt("curr_rssi");
				res.add(obj);
			}
			;
		} catch (SQLException e) {
			System.out.println("SQL查询语句异常。。");
			e.printStackTrace();
		}
		finally{
			CloseConn.closeConn(conn, ps, rs);
		}

		return res;
	}

	/**
	 * 主要是实现数据表的插入功能
	 * 
	 * @param obj
	 *            要插入数据库表中的对象
	 * @return 插入是否完成的状态，如果插入成功返回true，否则返回false。
	 */
	public int insert(UserObj obj) {
		// 定义插入成功后返回的成功记录数
		int result = 0;
		// 新建一个数据库连接对象
		JDBCMysql dbUtil = new JDBCMysql();
		// 定义一个连接
		Connection conn = dbUtil.getConn();
		// 定义要操作的SQL语句；
		String sql = "insert into user_infor(ap_id,user_mac,curr_time,curr_rssi) values(?,?,?,?)";
		PreparedStatement ps = null;
		
		try {
			// 准备好SQL语句
			ps = conn.prepareStatement(sql);
			// 设置要插入的数据
			ps.setInt(1, obj.apID);
			ps.setString(2, obj.UserMAC);			
			ps.setTimestamp(3, obj.currentTime);
			ps.setInt(4, obj.rssi);

			// 执行数据库的插入操作
			result = ps.executeUpdate();
		} catch (SQLException e) {
			System.out.println("SQL插入语句出现错误");
			e.printStackTrace();
		}
		finally{
			
				try {
					if(conn != null)
						conn.close();
					if(ps != null)
						ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
		}

		return result;
	}
	
	/**
	 * 通过查询数据库表中的 trigger 所触发的动作引发的状态。这里用数字表示，如果表变化以此insert_trigger表中对应
	 * 的项就加一，然后对比俩次查询的结果确定所要监控的表是否发生变化
	 * @param id 要查看表的变化，目前id=1 代表用户表的情况
	 * @return 当前表示对应表变化状态的整形数值。
	 */
	public  int getTriggerState(int id){
		int res = -1;
		JDBCMysql dbUtil = new JDBCMysql();
		Connection conn = dbUtil.getConn();
		String sql = "SELECT num FROM insert_trigger WHERE id=?";
		PreparedStatement ps =null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while (rs.next()){
				res = rs.getInt("num");
			}
		} catch (SQLException e) {
			System.out.println("SQL 异常。。");
			e.printStackTrace();
		}
		finally{
			CloseConn.closeConn(conn, ps, rs);
		}
		return res;
	}

	public static void main(String args[]) {
		UserDao snmp = new UserDao();
//		ArrayList<UserObj> result = snmp.select();
//		for (UserObj res : result) {
//			System.out.println(res.apID+" "+ res.currentTime+" "+res.rssi);
//		}

		UserObj obj = new UserObj();
		obj.UserMAC = "74-AD-B7-7F-CD-DE";
		String apmac = "00-56-C0-00-0A";
		APDao apDao =new APDao();
		obj.apID=apDao.getAPID(apmac);
		Date date =new Date();
		Timestamp timestamp = new Timestamp(date.getTime());
		obj.currentTime = timestamp;
		obj.rssi = -60;
		int i = snmp.insert(obj);
		if (i > 0)
			System.out.println("success");
		else {
			System.out.println("failed!");
		}

	}
}
