package com.control.mac;

import java.io.*;
import java.util.regex.*;
import java.util.*;

import com.dao.PhoneDao;
import com.obj.MacObj;
import com.obj.PhoneObj;

/**
 * 输入一个MAC地址，然后查找文件，将该MAC地址对应的企业名称找到并解析出来。 文件中还有企业的注册的详细地址
 * 
 * @author ZYK
 *
 */
public class SearchFile {
	public static void main(String arg[]) {

		String mac = "74-AD-B7-7F-CD-DE";
		// String mac = "c4-6a-b7-e9-ba-00";
		// String mac = "74-51-BA-5E-99-DB";
		
		ArrayList<MacObj> macObjs = new ArrayList<MacObj>();
		//ArrayList<String> macs = new ArrayList<String>();
		
		ReadFile rf = new ReadFile();
		ArrayList<String>  macs = rf.readFile();
		
		for(int i = 0; i < macs.size(); i++){
			MacObj macObj = new MacObj();
			macObj = macSearch(macs.get(i));
			macObjs.add(macObj);
		}
 		PhoneDao phoneDao =new PhoneDao();
 		boolean flags = phoneDao.insert(macObjs);
 		if(!flags){
 			System.out.println("mac与中文名称对照插入失败");
 		}
 		else {
			System.out.println("插入完成。。。。");
		}
	}

	public static MacObj macSearch(String mac) {
		MacObj macObj = new MacObj();  //定义要返回的结果对象
		
		List<String> strList = new ArrayList<String>();// 定义一个List存储读取的文本内容
		int flags = 0;// 定义一个flag存储keyword出现的行
		// 从给定的MAC地址中取出代表企业的关键部分；
		String keyword = mac.substring(0, 8);
		Pattern p = Pattern.compile(keyword);// 调用Pattern的compile方法编译要匹配的正则
		Matcher m;
		int i = 0;
		try {
			// MAC地址与企业名称对照文件
			// "./"表示当前目录了 ; "../表示上一目录";
			Reader re = new FileReader(new File("./res/oui.txt"));
			BufferedReader bre = new BufferedReader(re);
			while (bre.ready()) {
				String str = bre.readLine();
				strList.add(str);
				m = p.matcher(str);
				if (m.find())// 查找正则匹配的子串是否存在
				{
					flags = i;// 记录匹配的行
					break;
				}
				i++;
			}
			String result;
			// 首先将在给定文件 oui.txt 中查找mac，如果找到，输出厂商名称；
			// 如果没有查找到，转为在线查找；
			if (flags != 0) {
				result = strList.get(flags).substring(20);

				//System.out.println("MAC：" + mac + "属于： " + result);
			} else {
				// 在线查找，此方式查找速度比较慢
				result = MacGet.captureHtml(mac);
			}
			// 获取主流手机中英文对照表
			ArrayList<PhoneObj> objs = new ArrayList<PhoneObj>();
		//	objs = SystemInit.phones;
			
			//获取手机排行中的中英文对照数据
			PhoneDao phoneObj = new PhoneDao();
			objs = phoneObj.getPhoneinfor();
		//	System.out.println(objs.size());
			for (int j = 0; j < objs.size(); j++) {
				String key = objs.get(j).en_des;
				// 不区分大小写进行匹配 Pattern.CASE_INSENSITIVE
				Pattern pattern = Pattern.compile(key, Pattern.CASE_INSENSITIVE);
				Matcher matcher = pattern.matcher(result);
				if (matcher.find()) {					
					macObj.mac = mac;
					macObj.des = objs.get(j).ch_des;	
					
				}				
			}
			if(macObj.mac == null){
				macObj.mac = mac;
				macObj.des = "其它";
			}

		//	System.out.println(macObj.des);
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return macObj;
	}
	
	
}
