package com.control.mac;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author ZYK
 * 主要是从文件中多行数据中获取自己需要的部分，使用字符串匹配和分割技术来实现
 * */
public class ReadFile {
	
	public ArrayList<String> readFile() {
		ArrayList<String> res = new ArrayList<String>();
		String key = "Hex-STRING: "; //以该字符串为键值进行查询所需要数据的位置。
		Pattern pattern = Pattern.compile(key);
		Matcher m;
		try {
			Reader re = new FileReader(new File("./res/ip mac.txt")); //文件路径
			BufferedReader br = new BufferedReader(re);

			while (br.ready()) {
				String str = br.readLine();
				m = pattern.matcher(str);
				if (m.find()) {
					int temp = str.indexOf(key);//获得key出现的首位置索引
					int index = temp + key.length();//获得所需字符串出现的首位置索引
					String string = str.substring(index, str.length() - 1);
					//将“74 AD B7 7F CD DE”转换为“74-AD-B7-7F-CD-DE”，且长度为17.
					if (string.replace(' ', '-').length() == 17)
						res.add(string.replace(' ', '-'));
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;

	}

	public static void main(String args[]) {
		ReadFile rf = new ReadFile();
		ArrayList<String> rs = rf.readFile();
		for (int i = 0; i < rs.size(); i++) {
			System.out.println(rs.get(i));
		}
	}

}
