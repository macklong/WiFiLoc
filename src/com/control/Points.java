package com.control;

import java.util.ArrayList;

import com.dao.APDao;
import com.dao.UserDao;
import com.dist.Location;
import com.obj.APobj;
import com.obj.PointObj;
import com.obj.UserObj;

public class Points {

	public ArrayList<PointObj> getPoints(String mac) {
		// 定义返回结果列表
		ArrayList<PointObj> res = new ArrayList<PointObj>();

		// //查询所有终端用户的mac地址，然后依据每个用户的mac地址再查询各自在每个AP点的时间，信号强度等信息
		// MacDao macDao = new MacDao();
		// ArrayList<String> userMacList = macDao.lsitMAC();

		// 查询终端用户信息到objs 对象中
		UserDao userDao = new UserDao();
		ArrayList<UserObj> objs = userDao.select(mac);
		APDao apDao = new APDao();
		ArrayList<APobj> apobjs = apDao.getApInfor(mac);
		for (int i = 0; i < objs.size(); i++) {
			int id = objs.get(i).apID;
			PointObj point = apDao.getLonLat(id);

			Location location = new Location();

			PointObj p = new PointObj();
			for (int j = 0; j < apobjs.size(); j++) {
				if (apobjs.get(j).id == id) {
					p.lon = apobjs.get(j).point.lon;
					p.lat = apobjs.get(j).point.lat;
				}
			}
			PointObj temp = location.getLocation(p, -objs.get(i).rssi);
			res.add(temp);
		}
		return res;
	}
}
