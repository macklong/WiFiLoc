package com.obj;
/**
 * @author ZYK
 *主要定义一个接入终端用户的对象，用来表示连接到一个AP的终端的各种相关信息
 *信息有：mac地址，信号强度，AP的标识，时间等信息。
 *  */
public class UserObj {
	
	/**
	 *当前终端的MAC地址 
	 */
	public String UserMAC = "";	
	/**
	 * 该终端当前时刻的的信号强度
	 */
	public int rssi;	
	/**
	 * 当前时间
	 */
	public java.sql.Timestamp currentTime;
	/**
	 * 接入点AP的标识
	 */
	public int  apID = 0;
	
}
