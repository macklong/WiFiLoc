package com.obj;

public class PointObj {
	/**
	 *  经度
	 */
	public double lon = 0;
	/**
	 * 维度
	 */
	public double lat = 0;
	/**
	 * 这个变量只有在三角定位中才会使用，用来存储每个AP检测到的终端用户的RSSI
	 */
	public int rssi;
}
