package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sun.nio.cs.ext.MacUkraine;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.dao.MacDao;
import com.listener.SystemInit;
import com.obj.MacNum;

/**
 * Servlet implementation class MacServlet
 */
@WebServlet("/MacServlet")
public class MacServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
//	private ArrayList<MacNum> macNums = new ArrayList<MacNum>();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MacServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /* servlet 初始化函数
     * @see javax.servlet.GenericServlet#init()
     */
    public void init() throws ServletException{
//    	//从数据库获取数据
//    	MacDao macDao = new MacDao();
//    	macNums = macDao.getMACStatistic();
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		JSONArray jsonArray = new JSONArray();
		ArrayList<MacNum> macNums =SystemInit.macNums;
		for (int i = 0; i < macNums.size(); i++) {
			JSONObject object = new JSONObject();
			object.put("des", macNums.get(i).ch_des);
			object.put("count", macNums.get(i).count);
			jsonArray.add(object);
		}
		PrintWriter out = response.getWriter();
		out.print(jsonArray);
	}

}
