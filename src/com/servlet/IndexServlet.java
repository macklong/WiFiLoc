package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import test.Test;

import com.control.Points;
import com.listener.SystemInit;
import com.mysql.JDBCMysql;
import com.obj.PointObj;
import com.obj.sort.ComparePoint;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Servlet implementation class IndexServlet
 */
@WebServlet("/IndexServlet")
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

//	private ArrayList<PointObj> points;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public IndexServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/*
	 * 通过调用 init () 方法进行初始化。该方法被设计成只调用一次。 它在第一次创 建 Servlet 时被调用，在后续每次用户请求时不再调用;
	 * 因此在这里加载数据库信息，可以加速缓冲.
	 * 
	 * @see javax.servlet.GenericServlet#init()
	 */
	public void init() throws ServletException {
		System.out.println(" indexServlet init ....");
//		// 获得数据
//		Points p = new Points();
//		points = p.getPoints("74-AD-B7-7F-CD-DE");
//		Collections.sort(points, new ComparePoint());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// 设置response的编码格式
		response.setCharacterEncoding("UTF-8");
		// //通过html中对于的name 值来获取传回来的数据。这里的 “name” 可以看做要请求的值的key
		// String name = request.getParameter("name");
		// System.out.println(name);
		sendPoints(request, response);
		System.out.println("sending");

	}

	public void sendPoints(HttpServletRequest request,
			HttpServletResponse response) {
		// 定义一个json数组=，用来存放要传递的数据
		JSONArray data = new JSONArray();
		// //获得数据
		ArrayList<PointObj> points = SystemInit.points;
		// 定义json对象，用来表示数据的一个组
		JSONObject obj = new JSONObject();
		// 将points中的数据依次加入到JSONArray中
		for (int i = 0; i < points.size(); i++) {
			obj.put("lon", points.get(i).lon);
			obj.put("lat", points.get(i).lat);
			data.add(obj);
			obj.clear();
		}
		try {
			// 传送数据
			PrintWriter out = response.getWriter();
			out.print(data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
