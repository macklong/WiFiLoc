package h3c;

import java.io.IOException;

import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.Null;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;

/**
 * 实现snmp的walk操作
 * 
 * @author ZYK
 *
 */
public class SnmpWalk {
	private static int version = SnmpConstants.version2c;
	private static String protocol = "udp";
	private static int port = 161;

	
	/**
	 * 通过getnext的方式实现walk操作，来完成整张表的查询
	 * 
	 * @param ip snmp服务器ip地址
	 * @param community 团体名（口令）
	 * @param targetOid 要获取信息的oid
	 */
	private void snmpWalk(String ip, String community, String targetOid) {
		String address = protocol + ":" + ip + "/" + port; // 拼接访问协议，地址和端口号
		OID targetOID = new OID(targetOid);// 构造oid
		PDU requestPDU = new PDU();
		requestPDU.setType(PDU.GETNEXT);// 设定访问类型为GETNEXT
		requestPDU.add(new VariableBinding(targetOID));

		CommunityTarget target = SnmpUtil.createCommunityTarget(address,
				community, version, 2 * 1000L, 3);
		TransportMapping transport = null;
		Snmp snmp = null;
		try {
			transport = new DefaultUdpTransportMapping();
			snmp = new Snmp(transport);
			transport.listen(); // 监听

			boolean finished = false; // 定义是否访问完成整张表的状态
			while (!finished) {
				VariableBinding vb = null;
				ResponseEvent response = snmp.send(requestPDU, target);
				PDU responsePDU = response.getResponse();

				if (null == responsePDU) {
					System.out.println("responsePDU == null");
					finished = true;
					break;
				} else {
					vb = responsePDU.get(0);
				}
				// check finish
				finished = checkWalkFinished(targetOID, responsePDU, vb);
				if (!finished) {
					System.out.println(vb.getOid().toString() + ": "
							+ vb.getVariable().toString());
					// Set up the variable binding for the next entry.
					requestPDU.setRequestID(new Integer32(0));
					requestPDU.set(0, vb);
				}
			}
			System.out.println("success finish snmp walk!");
		} catch (Exception e) {
			System.out.println("SNMP walk Exception: " + e);
		} finally {
			if (snmp != null) {
				try {
					snmp.close();
				} catch (IOException ex1) {
					snmp = null;
				}
			}
			if (transport != null) {
				try {
					transport.close();
				} catch (IOException ex2) {
					transport = null;
				}
			}
		}

	}

	/**
	 * 检查snmp walk 操作是否完成
	 * 
	 * @param resquestPDU
	 * @param targetOID
	 * @param responsePDU
	 * @param vb
	 * @return 操作完成与否的状态（true or false）
	 */
	private boolean checkWalkFinished(OID targetOID, PDU responsePDU,
			VariableBinding vb) {
		boolean finished = false; //是否完成的状态
		if (responsePDU.getErrorStatus() != 0) {//检查返回的错误索引，来判断错误状态
			System.out.println("responsePDU.getErrorStatus() != 0 ");
			System.out.println(responsePDU.getErrorStatusText());
			finished = true;
		} else if (vb.getOid() == null) { //判断oid是否为null
			System.out.println("vb.getOid() == null");
			finished = true;
		} else if (vb.getOid().size() < targetOID.size()) {
			System.out.println("vb.getOid().size() < targetOID.size()");
			finished = true;
			//比较targetOID的左边targetOID.size()位与vb.getoid()是否相同
		} else if (targetOID.leftMostCompare(targetOID.size(), vb.getOid()) != 0) {
			// System.out.println("targetOID.leftMostCompare() != 0"+targetOID.size());
			finished = true;
		} else if (Null.isExceptionSyntax(vb.getVariable().getSyntax())) {
			System.out
					.println("Null.isExceptionSyntax(vb.getVariable().getSyntax())");
			finished = true;
		} else if (vb.getOid().compareTo(targetOID) <= 0) {
			System.out
					.println("Variable received is not lexicographic successor of requested one");
			System.out.println(vb.toString() + " <= " + targetOID);
			finished = true;
		}
		return finished;

	}

	public static void main(String[] args) {
		String ip = "127.0.0.1";
		String community = "public";
		String targetOid = ".1.3.6.1.2.1.1";
		SnmpWalk tester = new SnmpWalk();
		tester.snmpWalk(ip, community, targetOid);

	}
}