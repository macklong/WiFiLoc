package h3c;

public class H3C_OID {
	
	
	/**
	 * 网络设备的全名和版本
	 */
	final public static String sysDescr = "1.3.6.1.2.1.1.1";
	/**
	 * 系统名称，这里应该是 “AC” 接入点控制服务器
	 */
	final public static String sysName = "1.3.6.1.2.1.1.5";
	/**
	 * 该索引值唯一地标识了某一条目所用的接口，如局域网端使用的接口是35
	 */
	final public static String ipAdEntIfIndex = ".1.3.6.1.2.1.4.20.1.2";
	/**
	 * 终端接入对应的IP地址
	 */
	final public static String ipNetToMediaNetAddress = "1.3.6.1.2.1.4.22.1.3.220";
	/**
	 * 所有IP对应的MAC地址，这里的220
	 */
	final public static String ipNetToMediaPhysAddress = "1.3.6.1.2.1.4.22.1.2.220";
	/**
	 * 无线网的索引
	 */
	final public static String  h3cDot11RRMRadioIndex = ".1.3.6.1.4.1.2011.10.2.75.8.2.1.1.1";
	/**
	 * 无线接入点AP的ID
	 */	
	final public static String h3cDot11CurrAPElementID = ".1.3.6.1.4.1.2011.10.2.75.2.1.2.1.15";
	/**
	 * 无线接入点AP的IP地址
	 */
	final public static String h3cDot11CurrAPIPAdress = ".1.3.6.1.4.1.2011.10.2.75.2.1.2.1.2";
	/**
	 * 无线接入点AP的MAC地址
	 */
	final public static String h3cDot11CurrAPMacAddress = ".1.3.6.1.4.1.2011.10.2.75.2.1.2.1.3";
	/**
	 * 无线接入点AP信号的强度
	 */
	final public static String h3cDot11RrmNbrRSSI = ".1.3.6.1.4.1.2011.10.2.75.8.2.2.1.4";
}
