/**
 * Created by ZYK on 14-6-26.
 */
function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=")
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1
            c_end = document.cookie.indexOf(";", c_start)
            if (c_end == -1) c_end = document.cookie.length
            return unescape(document.cookie.substring(c_start, c_end))
        }
    }
    return ""
}
function deleteCookie(name) {
    document.cookie = name + "=;expires=" + (new Date(0)).toGMTString();
}
function setCookie(c_name, value, expiredays) {
    var exdate = new Date()
    exdate.setDate(exdate.getDate() + expiredays)
    document.cookie = c_name + "=" + escape(value) +
        ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString())
}

function checkCookie(name) {
    deleteCookie('username');
    deleteCookie('password');
    deleteCookie("id");
    deleteCookie('role');

    username = getCookie('username')
    if (username != null && username != "") {
        alert('Welcome again ' + username + '!')
    }
    else {
        username = name;
        id = $("#name").val();
        password = $("#password").val();
        role = $("input[name='who']:checked").val()
        if (role == "admin") {
            role = "管理员";
        }
        else if (role == "student") {
            role = "学生";
        } else if (role == "teacher") {
            role = "教师"
        }

        if (username != null && username != "") {
            setCookie('username', username, 1)
            setCookie('id', id, 1)
            setCookie('password', password, 1)
            setCookie('role', role, 1)
        }
    }
}